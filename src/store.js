import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        cartCount: 0,
        totalPrice: 0,
        foundItemIndex: 0,
        cartproducts: [],
        cartquantities: [],
        order: {}
    },
    mutations: {
        addCartItem: async function (state, i) {
            if (state.cartCount == 0) {
                state.cartproducts.push(i);
                state.cartquantities.push(1);
                state.cartCount += 1;
            }
            else {
                let seekItem = state.cartproducts.find((element) => {
                    return element._id == i._id
                })
                if (seekItem == undefined) {
                    state.cartproducts.push(i);
                    state.cartquantities.push(1);
                    state.cartCount += 1;
                }
                else {
                    state.cartquantities[state.cartproducts.indexOf(seekItem)] += 1;
                    state.cartCount += 1;
                }
            }
        },
        addQuantity: (state, item) => {
            state.cartquantities[state.cartproducts.indexOf(item)] += 1;
            state.cartCount += 1;
        },
        subQuantity: (state, item) => {
            if (state.cartquantities[state.cartproducts.indexOf(item)] > 1) {
                state.cartquantities[state.cartproducts.indexOf(item)] -= 1;
                state.cartCount -= 1;
            }
        },
        clearCart: (state) => {
            state.cartproducts.length = 0
            state.cartquantities.length = 0
            state.cartCount = 0
            console.log("cart cleared.")
            state.totalPrice = 0
        },
        countTotalPrice: (state) => {
            let i=0;
            state.totalPrice=0;
            while (i < state.cartproducts.length) {
                state.totalPrice += state.cartquantities[i] * state.cartproducts[i].price;
                console.log(state.cartquantities[i])
                i++;
            }
        }

    },
    getters: {
        getCartCount: (state) => {
            return state.cartCount
        },
        getCartList: (state) => {
            return state.cartproducts
        },
        getCartQuantities: (state) => {
            return state.cartquantities
        },
        getTotalPrice: (state) => {
            return state.totalPrice
        },
    }
})
export default store;
